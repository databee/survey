<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colours = [
            '408fec',
            '79589f',
            '74c080',
            'fa9f47',
            'd64242'
        ];

        $universities = [
            [
                'code'  => 'CSU',
                'name'  => 'Charles Sturt University',
                'alias' => 'Large Multi-campus Regional University',
                'exam_events'   => 3,
                'exams_per_event'   => 700,
                'sittings_per_event'    => 35000,
                'staff'                 => '3',
                'staff_description'     => '3 part time staff - role split between exams and hecs. 1 TL split responsibility'
            ],
            [
                'code'  => 'CDU',
                'name'  => 'Charles Darwin University',
                'alias' => 'Small Suburban University',
                'exam_events'   => 5,
                'exams_per_event'   => 110,
                'sittings_per_event'    => 8000,
                'staff'                 => 3,
                'comments'              => 'Databee saves energy on end to end process mgt. What you save in one place you may need to spend a little of in another in transition of business process. We don\'t have a fully automated system, so have not reached the full gain possible - but what we have gained is huge. Rates of pay with supervisors don\'t mesh well with our systems. Databee has significantly changed our exam practice in a healthy way. Our biggest win has been the visibility on our practice we can finally see what we have managed for years manually.'
            ],
            [
                'code'  => 'FED',
                'name'  => 'Federation University',
                'alias' => 'Small Regional University',
                'exam_events'   => 6,
                'exams_per_event'   => 330,
                'exams_per_event_description'   => 'Approx 330 in each Sem 1 & 2, plus approx 110 each def/sup period, approx 10 in Summer Semester.',
                'sittings_per_event'    => 12000,
                'staff'                 => 2
            ],
            [
                'code'  => 'UniSA',
                'name'  => 'University of South Australia',
                'alias' => 'Inner-city University',
                'exam_events'   => 8,
                'exams_per_event'   => 0,
                'exams_per_event_description'   => 'Exams managed in large venues, so this varies.  Largest average group of exams is 10',
                'sittings_per_event'    => 0,
                'sittings_per_event_description'    => 'Venue 1: 1004 student seat capacity; Venue 2: 194 student seat capacity',
                'staff'                 => 5,
                'staff_description'     => '5 (1xManager; 1xBusiness Analyst; 1x Exam Officer; 2x Exam Advisors)',
                'comments'              => 'I really can\'t answer this survey in a way that\'s fair to the Databee product. Prior to Databee our exams office was quite a limited entity, that only has one casual person coordinating the bare basics of 2 main exam events (scheduling, & supervisors-in-charge). 

Exams Office functions were very limited by the software that we had for the 10 years prior. I didn\'t collect papers for printing, I didn\'t collate attendance data. We did not run off-campus exams. Our implementation of Databee also coincided with the commencement of Online Learning here, resulting in a new need for facilitating off-campus exams.

I schedule 4 times more supervisors, AAA students & venues now that we have Databee. We coordinate students sitting at 45+ external exam centers, and communicate exam timetables directly to students email now. 

Since Databee, the exams processes have centralized, and we gather requirements & papers directly from Course Coordinators each semester.

Despite these additional duties and increase in numbers, we still only have:

- 1 full-time permanent person fully coordinating exams operations
- 1 almost full-time casual exams assistant
- 1 additional full-time casual for around 8-10 weeks for each main exam period.

We handle all requirements gathering, scheduling, timetable publications, paper printing & logistics (including packing), off-campus exams, AAA adjustments and sittings, supervisors, recording attendance, recording incidents and enquiries, etc.

So my "before" Databee answers really can\'t translate directly to our "after Databee" processes - if that makes sense. I am also happy to discuss any Databee related questions you may have at any time.'
            ],
            [
                'code'  => 'UQ',
                'name'  => 'The University of QLD',
                'alias' => 'Large \'Group of Eight\' University',
                'exam_events'   => 8,
                'exams_per_event'   => 830,
                'exams_per_event_description'   => '830 exams across 46 exam sessions in a 2week exam block including Saturdays',
                'sittings_per_event'    => 100000,
                'sittings_per_event_description'    => '2,300 per exam session. 100,000 sittings in total',
                'staff'                 => 7,
                'comments'              => 'Clarification for some of our responses:
                
Exam Master Preparations: this is was managed by exam team, Schools and external printer, however Databee processes have reduced paper preparation tasks required by Schools and exam team. Accuracy and consistency of information on exam paper coversheets has increased. Exam paper security has increased. Exam paper tracking is automated with barcode scanning.

Incident reporting: although it appears no time saved, ability to extract data for reporting is a bonus. We are still reviewing this process as manual data entry of each report is required.

Recording attendance: this has been answered with external student exam processes in mind. We have not implemented barcode scanning of id cards for internal students yet.

Overall, Databee has proven to be a positive step providing an increase in efficiency and effectiveness of processes across the whole spectrum: exam data collection; data transfer from Peoplesoft; managing concurrent exams; exam allocation; timetable extraction/formatting/publication; external student management; alternative exam management; exam paper print management and tracking; invigilator rostering and communication; reporting; student communications.'
            ],
            [
                'code'  => 'CUR',
                'name'  => 'Curtin University',
                'alias' => 'Large Suburban University',
                'exam_events'   => 4,
                'exams_per_event'   => 800,
                'sittings_per_event'    => 70000,
                'staff'                 => 6,
                'comments'              => 'The Databee system has significantly improved efficiency across all areas of Exams. There are significant benefits from using Databee as compared to the Microsoft access databases which were used for alternative exam arrangements, off campus/external arrangements, excel spreadsheets for supervisor rostering and AdAstra for timetable scheduling. Databee includes daily import of enrolment, student and course data and significantly reduces the manual tasks which were undertaken when using the previous Microsoft access databases/AdAstra systems.

We were also able to centralise operations across multiple campuses which resulted in a reduction of one FTE (full time employee) from one campus. 

We can now run multiple exam events and now have multi user access to each exam event which enables cross skilling across the various exam processes.'
            ],
            [
                'code'  => 'USC',
                'name'  => 'University of the Sunshine Coast',
                'alias' => 'Small Suburban University',
                'exam_events'   => 4,
                'exams_per_event'   => 150,
                'exams_per_event_description'   => 'SEM1 – 150, SEM 1 DE/SU – 108, SEM 2 – 159, SEM2 DE/SU and Summer – 105',
                'sittings_per_event'    => 16000,
                'sittings_per_event_description'    => 'SEM1 – 16196, SEM 1 DE/SU – 628, SEM 2 – 13,901, SEM2 DE/SU and Summer – 945',
                'staff'                 => 3,
                'comments'              => 'Exams Manager removes the manual intervention of processes, instead allowing the user to enter the information (i.e. distributed work instead of concentrated work). The big time savers are the set up of your exams, requests for exams, roll over of alternative exams as well as the online upload of exam papers.'
            ],
        ];

        foreach($universities as $university)
        {
            \App\University::create($university);
        }

        $questions = [
            [
                'question'  => [
                    'title' => 'Collecting and Entering Exam Details',
                    'subtitle'  => 'Universities were asked "How long did collecting and entering examination details take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did collecting and entering examination details take prior to adopting Databee?',
                    'after_duration'    => 'How long does collecting and entering examination details take after adopting Databee?',
                    'promo_text'    => 'The Online Exam Request process allows faculty staff to take responsibility for entering their own exam details. This includes duration, allowed materials, and other information that would otherwise have to be entered into a records system manually.'
                ],
                'answers'   => [
                    'CSU'   => [
                        'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                        'before_staff'  => 5,
                        'after_duration_lower' => 14, 'after_duration_upper'  => 21,
                        'after_staff'  => 2,
                    ],
                    'CDU'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 2,
                        'after_duration_lower' => 5, 'after_duration_upper'  => 5,
                        'after_staff'  => 1,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 5,
                        'after_duration_lower' => 14, 'after_duration_upper'  => 21,
                        'after_staff'  => 5,
                    ],
                    'UQ'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 1,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                        'before_staff'  => 5,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 5,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 3,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Scheduling the Exam Timetable',
                    'subtitle'  => 'Universities were asked "How long did scheduling the examination timetable take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did scheduling the examination timetable take prior to adopting Databee?',
                    'after_duration'    => 'How long does scheduling the examination timetable take after adopting Databee?',
                    'promo_text'    => 'Exams Manager\'s automated exam allocation process is designed to give the best possible timetable for your students. Exams Manager doesn\'t need you to specify stringent rules about the timetabling process — it simply produces the most even, fair timetable it can with the resources you have available. You can easily identify and resolve any timetabling issues and optimise your timetable prior to publication.'
                ],
                'answers'   => [
                    'CSU'   => [
                        'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                        'before_staff'  => 1,
                        'after_duration_lower' => 4, 'after_duration_upper'  => 4,
                        'after_staff'  => 1,
                    ],
                    'CDU'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 2,
                        'after_duration_lower' => 4, 'after_duration_upper'  => 4,
                        'after_staff'  => 2,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 1,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 1,
                    ],
                    'UniSA'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 1,
                        'after_duration_lower' => 4, 'after_duration_upper'  => 4,
                        'after_staff'  => 1,
                    ],
                    'UQ'   => [
                        'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                        'before_staff'  => 1,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 1,
                    ],
                    //'CUR'   => [
                    //    'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                    //    'before_staff'  => 5,
                    //    'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                    //    'after_staff'  => 1,
                    //],
                    'USC'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 5,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 1,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Scheduling Special Conditions Exams',
                    'subtitle'  => 'Universities were asked "How long did scheduling special conditions exams take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did scheduling special conditions exams take prior to adopting Databee?',
                    'after_duration'    => 'How long did scheduling special conditions exams take after adopting Databee?',
                    'promo_text'    => 'Special conditions can be managed by your disability support staff online, or imported directly from your student record system. This saves hours of data entry for your exams team and significantly reduces the risk of errors. Exams can be individually or bulk allocated. The bulk allocation process allows you to automatically create special exams for a selected group of students, and takes into account the students\' venue and equipment requirements, and any extended exam durations.'
                ],
                'answers'   => [
                    'CSU'   => [
                        'before_duration_lower' => NULL, 'before_duration_upper'  => NULL,
                        'before_staff'  => 2,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 1,
                    ],
                    'CDU'   => [
                        'before_duration_lower' => 3, 'before_duration_upper'  => 3,
                        'before_staff'  => 2,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 3, 'before_duration_upper'  => 3,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'UniSA'   => [
                        'before_duration_lower' => 3, 'before_duration_upper'  => 3,
                        'before_staff'  => 1,
                        'after_duration_lower' => 3, 'after_duration_upper'  => 3,
                        'after_staff'  => 1,
                    ],
                    'UQ'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                        'before_staff'  => 1,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 1,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 2,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Examination Masters',
                    'subtitle'  => 'Universities were asked "How long did receiving examination papers, organising, logging, and preparing them for printing take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did receiving examination papers, organising, logging, and preparing them for printing take prior to adopting Databee',
                    'after_duration'    => 'How long does receiving examination papers, organising, logging, and preparing them for printing take after adopting Databee?',
                    'promo_text'    => 'Databee enables the electronic upload of exam papers to a secure server, online exam paper approval and secure downloading of exams papers for printing. There is even a secure option to allow your printing service provider to manage exam paper printing themselves, reducing the handling of exam papers and saving your exams team hours of work.'
                ],
                'answers'   => [
                    'CSU'   => [
                        'before_duration_lower' => 35, 'before_duration_upper'  => 35,
                        'before_staff'  => 5,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 1,
                    ],
                    'CDU'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 2,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 2,
                    ],
                    'FED'   => [
                        'before_duration_lower' => NULL, 'before_duration_upper'  => NULL,
                        'before_staff'  => NULL,
                        'after_duration_lower' => 5, 'after_duration_upper'  => 5,
                        'after_staff'  => 1,
                    ],
                    //'UniSA'   => [
                    //    'before_duration_lower' => 3, 'before_duration_upper'  => 3,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 3, 'after_duration_upper'  => 3,
                    //    'after_staff'  => 1,
                    //],
                    'UQ'   => [
                        'before_duration_lower' => 28, 'before_duration_upper'  => 35,
                        'before_staff'  => 5,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 3,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 5,
                        'after_duration_lower' => 5, 'after_duration_upper'  => 5,
                        'after_staff'  => 2,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 5,
                        'after_duration_lower' => 5, 'after_duration_upper'  => 5,
                        'after_staff'  => 2,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Exam Supervisor Rostering',
                    'subtitle'  => 'Universities were asked "How long did rostering exam supervisors take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did rostering exam supervisors take prior to adopting Databee',
                    'after_duration'    => 'How long does exam supervisor rostering take after adopting Databee?',
                    'promo_text'    => 'Supervisors can submit their availability online, which minimises time consuming and error prone back and forth correspondence for your exams team. The automated supervisor scheduling only takes a few minutes to run and will evenly distribute sessions among your supervisors, taking into account their skill level, gender, any restrictions and preferences. Supervisor rosters are provided and accepted online.'
                ],
                'answers'   => [
                    'CSU'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 1,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ],
                    'CDU'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 5,
                        'after_duration_lower' => 5, 'after_duration_upper'  => 5,
                        'after_staff'  => 1,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 2, 'before_duration_upper'  => 2,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'UniSA'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'UQ'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 1,
                        'after_duration_lower' => 3, 'after_duration_upper'  => 3,
                        'after_staff'  => 1,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 1,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 1,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 1,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Exam Incident and Misconduct Report Processing',
                    'subtitle'  => 'niversities were asked "How long did processing exam incident and misconduct reports take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did processing exam incident and misconduct reports take prior to adopting Databee?',
                    'after_duration'    => 'How long does processing exam incident and misconduct reports take after adopting Databee?',
                    'promo_text'    => 'Exams Manager can track incidents that occur during the exam period - everything from cheating to disruptions to evacuations. You can store photographs and scans of confiscated cheat notes. The automated correspondence system makes informing the relevant staff a quick and easy task.'
                ],
                'answers'   => [
                    //'CSU'   => [
                    //    'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                    //    'after_staff'  => 1,
                    //],
                    'CDU'   => [
                        'before_duration_lower' => NULL, 'before_duration_upper'  => NULL,
                        'before_staff'  => NULL,
                        'after_duration_lower' => 3, 'after_duration_upper'  => 3,
                        'after_staff'  => 1,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 1, 'before_duration_upper'  => 1,
                        'before_staff'  => 1,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ],
                    'UniSA'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 1,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 1,
                    ],
                    'UQ'   => [
                        'before_duration_lower' => 4, 'before_duration_upper'  => 4,
                        'before_staff'  => 1,
                        'after_duration_lower' => 4, 'after_duration_upper'  => 4,
                        'after_staff'  => 1,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => NULL, 'before_duration_upper'  => NULL,
                        'before_staff'  => NULL,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 2,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 1, 'before_duration_upper'  => 1,
                        'before_staff'  => 1,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Exam Supervisor Pay Claim Processing',
                    'subtitle'  => 'Universities were asked "How long did processing exam supervisor pay claims take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did processing exam supervisor pay claims take prior to adopting Databee?',
                    'after_duration'    => 'How long does processing exam supervisor pay claims take after adopting Databee?',
                    'promo_text'    => 'Exams Manager can calculate the payment due for each of your Supervisors. It can produce reports and export timesheet data to various HR systems.'
                ],
                'answers'   => [
                    'CSU'   => [
                        'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                        'before_staff'  => 2,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 1,
                    ],
                    'CDU'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 5,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 2,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 1, 'before_duration_upper'  => 1,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'UniSA'   => [
                        'before_duration_lower' => 0.5, 'before_duration_upper'  => 0.5,
                        'before_staff'  => 1,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ],
                    //'UQ'   => [
                    //    'before_duration_lower' => 4, 'before_duration_upper'  => 4,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 4, 'after_duration_upper'  => 4,
                    //    'after_staff'  => 1,
                    //],
                    'CUR'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 2,
                        'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                        'after_staff'  => 1,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 2, 'before_duration_upper'  => 2,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Recording Exam Attendance',
                    'subtitle'  => 'Universities were asked "How long did recording attendance data take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did recording attendance data take prior to adopting Databee?',
                    'after_duration'    => 'How long does recording attendance data take after adopting Databee?',
                    'promo_text'    => 'Confirmation of student attendance is recorded quickly and easily by scanning barcodes on student attendance sheets. A website allows academic and general staff to view student attendance immediately after it is scanned.'
                ],
                'answers'   => [
                    //'CSU'   => [
                    //    'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                    //    'after_staff'  => 1,
                    //],
                    'CDU'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 5,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 2,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 2, 'before_duration_upper'  => 2,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    //'UniSA'   => [
                    //    'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                    //    'after_staff'  => 1,
                    //],
                    'UQ'   => [
                        'before_duration_lower' => 2, 'before_duration_upper'  => 2,
                        'before_staff'  => 1,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => NULL, 'before_duration_upper'  => NULL,
                        'before_staff'  => NULL,
                        'after_duration_lower' => 5, 'after_duration_upper'  => 5,
                        'after_staff'  => 1,
                    ],
                    //'USC'   => [
                    //    'before_duration_lower' => 1, 'before_duration_upper'  => 1,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                    //    'after_staff'  => 1,
                    //]
                ]
            ],
            [
                'question'  => [
                    'title' => 'Receipting of Completed Exam Scripts',
                    'subtitle'  => 'Universities were asked "How long did receipting of completed exam papers take prior to and after adopting Databee?".',
                    'before_duration'   => 'How long did receipting of completed exam papers take prior to adopting Databee?',
                    'after_duration'    => 'How long does receipting of completed exam papers take after adopting Databee?',
                    'promo_text'    => 'Receipt of completed exam scripts is recorded quickly and easily by scanning barcodes. This also enables easy tracking of exam papers.'
                ],
                'answers'   => [
                    //'CSU'   => [
                    //    'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 1, 'after_duration_upper'  => 1,
                    //    'after_staff'  => 1,
                    //],
                    'CDU'   => [
                        'before_duration_lower' => 14, 'before_duration_upper'  => 21,
                        'before_staff'  => 5,
                        'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                        'after_staff'  => 3,
                    ],
                    'FED'   => [
                        'before_duration_lower' => 1, 'before_duration_upper'  => 1,
                        'before_staff'  => 2,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    //'UniSA'   => [
                    //    'before_duration_lower' => 5, 'before_duration_upper'  => 5,
                    //    'before_staff'  => 1,
                    //    'after_duration_lower' => 7, 'after_duration_upper'  => 14,
                    //    'after_staff'  => 1,
                    //],
                    'UQ'   => [
                        'before_duration_lower' => NULL, 'before_duration_upper'  => NULL,
                        'before_staff'  => NULL,
                        'after_duration_lower' => 0.5, 'after_duration_upper'  => 0.5,
                        'after_staff'  => 1,
                    ],
                    'CUR'   => [
                        'before_duration_lower' => 3, 'before_duration_upper'  => 3,
                        'before_staff'  => 1,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 1,
                    ],
                    'USC'   => [
                        'before_duration_lower' => 7, 'before_duration_upper'  => 14,
                        'before_staff'  => 5,
                        'after_duration_lower' => 2, 'after_duration_upper'  => 2,
                        'after_staff'  => 1,
                    ]
                ]
            ]
        ];

        foreach($questions as $question)
        {
            $q = \App\Question::create($question['question']);


            $q->colour = alternator('408fec',
                '79589f',
                '74c080',
                'fa9f47',
                'd64242');

            $q->save();

            if( ! empty($question['answers']))
            {
                foreach($question['answers'] as $key => $answers)
                {
                    $u = \App\University::where('code', $key)->first();

                    $answer = new \App\Answer($answers);

                    $answer->university_id = $u->id;
                    $answer->question_id = $q->id;

                    $answer->save();
                }
            }
        }
    }
}
