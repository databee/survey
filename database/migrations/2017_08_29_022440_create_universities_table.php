<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('alias');
            $table->integer('exam_events');
            $table->string('exam_events_description')->nullable();
            $table->integer('exams_per_event');
            $table->string('exams_per_event_description')->nullable();
            $table->integer('sittings_per_event');
            $table->string('sittings_per_event_description')->nullable();
            $table->integer('staff');
            $table->string('staff_description')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universities');
    }
}
