<?php

namespace App\Http\Controllers;

use App\Question;
use App\University;
use Illuminate\Http\Request;

class UniversitiesController extends Controller
{
    public function index()
    {
        $universities = University::get();

        return view('universities.index', compact('universities'));
    }

    public function show(University $university)
    {
        $university->load('answers', 'answers.question');

        // get the next and previous...
        $all = University::get();

        $previous = $next = $current = null;

        foreach($all as $u)
        {
            if( ! empty($current))
            {
                $next = $u;

                break;
            }

            if($u->id == $university->id)
            {
                $current = $u;
            } else
            {
                $previous = $u;
            }
        }

        return view('universities.show', compact('university', 'previous', 'next'));

    }
}
