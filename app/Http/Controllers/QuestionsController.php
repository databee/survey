<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{

    public function index()
    {
        $questions = Question::get();

        return view('questions.index', compact('questions'));
    }

    public function show(Question $question)
    {
        $question->load('answers', 'answers.university');

        // get the next and previous...
        $all = Question::get();

        $previous = $next = $current = null;

        foreach($all as $q)
        {
            if( ! empty($current))
            {
                $next = $q;

                break;
            }

            if($q->id == $question->id)
            {
                $current = $q;
            } else
            {
                $previous = $q;
            }
        }

        return view('questions.show', compact('question', 'previous', 'next'));

    }
}
