<?php

function lighten($hex, $amount = 10)
{
    $colour = new \Mexitek\PHPColors\Color($hex);

    return '#' . $colour->lighten($amount);
}

function darken($hex, $amount = 10)
{
    $colour = new \Mexitek\PHPColors\Color($hex);

    return '#' . $colour->darken($amount);
}