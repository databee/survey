<?php

/**
 * @param $string
 * @param $tagname
 * @return array
 */
function getTextBetweenTags($string, $tagname){
    $d = new DOMDocument();
    $d->loadHTML($string);
    $return = array();
    foreach($d->getElementsByTagName($tagname) as $item){
        $return[] = $item->textContent;
    }
    return $return;
}


if( ! function_exists('meta_title'))
{
    function meta_title()
    {
        return config('app.name');
    }
}

if( ! function_exists('parse_markdown'))
{
    function parse_markdown($str)
    {
        $str = trim($str);

        if (empty($str)) {
            return;
        }

        $environment = League\CommonMark\Environment::createCommonMarkEnvironment();
        $parser = new League\CommonMark\DocParser($environment);
        $htmlRenderer = new League\CommonMark\HtmlRenderer($environment);

        $text = $parser->parse($str);
        return $htmlRenderer->renderBlock($text);
    }
}

if( ! function_exists('alternator'))
{
    function alternator($args)
    {
        static $i;
        if (func_num_args() === 0) {
            $i = 0;

            return '';
        }
        $args = func_get_args();

        return $args[($i ++ % count($args))];
    }
}

if( ! function_exists('reading_time'))
{
    function reading_time($text)
    {
        return ceil(str_word_count($text) / 250);
    }
}