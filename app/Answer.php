<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded = [];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function relativeScale($prefix, $baseScale = 20)
    {
        $range = 100 - $baseScale;

        $increments = $range / 35;

        // Are the before/after durations the same? If so, use the upper figure regardless


        if($prefix == 'before')
        {
            $size = $this->getAttribute('before_duration_upper');
        } else
        {
            if($this->getAttribute('after_duration_lower') == $this->getAttribute('before_duration_lower') &&
                $this->getAttribute('after_duration_upper') == $this->getAttribute('before_duration_upper')
                )
            {
                $size = $this->getAttribute('after_duration_upper');
            } else
            {
                $size = $this->getAttribute('after_duration_lower');
            }
        }

        return round($baseScale + ($size * $increments));
    }

    public function duration($prefix)
    {
        $lower = $this->getAttribute($prefix . '_duration_lower');

        if(is_null($lower)) return 'Don\'t Know';

        $upper = $this->getAttribute($prefix . '_duration_upper');
        $unit = 'day';

        if($lower >= 7)
        {
            $lower = $lower / 7;
            $upper = $upper / 7;
            $unit = 'week';
        }

        if($lower != $upper)
        {
            return $lower . '-' . $upper . ' ' . str_plural($unit, $upper);
        } else
        {
            return $upper . ' ' . str_plural($unit, $upper);
        }
    }

    public function staff($prefix)
    {
        $staff = $this->getAttribute($prefix . '_staff');

        if(is_null($staff)) return 'Don\'t Know';

        if($staff > 4) $staff = '4+';

        return $staff;
    }

    public function totalDays($prefix)
    {
        $lower = $this->getAttribute($prefix . '_duration_lower');

        if(is_null($lower)) return 0;

        $upper = $this->getAttribute($prefix . '_duration_upper');

        $mean = ($lower + $upper) / 2;

        return $mean * $this->getAttribute($prefix . '_staff');
    }
}
