@extends('layouts.survey')

@section('content')
<section class="section-hero minor" style="background-color: #{{ $question->colour }};">
    <div class="hero-content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1>{{ $question->title }}</h1>

                @if($question->subtitle)
                <p class="lead">{{ $question->subtitle }}</p>
                @endif

                @if($question->promo_text)
                    <div class="row" style="_margin-top: 30px;">
                        <div class="col-md-10 col-md-offset-1">
                            <blockquote>
                                {{ $question->promo_text }}
                            </blockquote>

                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>

<section style="color: {{ darken('#' . $question->colour, 20) }}; background-color: {{ lighten('#' . $question->colour, 30) }}; padding-top: 15px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center">Before Implementing<br>Databee Exams Manager</div>
                    <div class="col-md-4 text-center">After Implementing<br>Databee Exams Manager</div>
                </div>
            </div>
        </div>
    </div>
</section>
            @foreach($question->answers as $answer)
                <section style="padding-top: 30px; padding-bottom: 15px; background-color: {{ alternator(lighten('#' . $question->colour, 30), lighten('#' . $question->colour, 35)) }}">


                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">


                <div class="row" style="display: flex; align-items: center">
                    <div class="col-md-4">
                        <h3 class="text-center" style="margin: 0">

                            <a href="/university/{{ $answer->university->id }}" style="color: {{ darken('#' . $question->colour, 20) }}">{{ $answer->university->alias }}</a>
                        </h3>
                    </div>

                    <div class="col-md-4" style="position: relative;">
                        @if(!empty($answer->before_duration_lower))
                            <div style="display: block;
                                    background: #{{ $question->colour }};
                                    border-radius: 100%;
                                    width: {{ $answer->relativeScale('before') }}%;
                                    margin: 0 auto;
                                    ">
                                <div style="padding: 50% 0;"></div>

                            </div>
                            <p class="lead text-center" style="font-size: 2em; position: absolute; margin: 0; top: 50%; left: 50%; transform: translate(-50%, -50%); color: #fff;">

                                @foreach(range(1, $answer->before_staff) as $index)
                                    @if($index == 4)<br />@endif
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>

                                @endforeach
                            </p>

                        @else
                            <div class="text-center" style="font-size: 3em; color: #{{ $question->colour }}">
                                <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                            </div>
                        @endif

                    </div>

                    <div class="col-md-4" style="position: relative;">
                        <div style="display: block;
                                background: #{{ $question->colour }};
                                border-radius: 100%;
                                width: {{ $answer->relativeScale('after') }}%;
                                margin: 0 auto;
                                ">
                            <div style="padding: 50% 0;"></div>

                        </div>

                        <p class="lead text-center" style="font-size: 2em; position: absolute; margin: 0; top: 50%; left: 50%; transform: translate(-50%, -50%); color: #fff;">
                            @foreach(range(1, $answer->after_staff) as $index)
                                @if($index == 4)<br />@endif
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>

                            @endforeach
                        </p>
                    </div>
                </div>

                        </div>
                    </div>



                    <div class="row text-center" style="color: {{ darken('#' . $question->colour, 20) }}; margin-top: 5px;">
                        <div class="col-md-10 col-md-offset-1">
                          <div class="row">
                              <div class="col-md-4 col-md-offset-4">
                                  @if(!empty($answer->before_duration_lower))
                                  {{ $answer->duration('before') }} / {{ $answer->staff('before') }} staff
                                  @endif
                              </div>

                              <div class="col-md-4">
                                  {{ $answer->duration('after') }} / {{ $answer->staff('after') }} staff
                              </div>
                          </div>

                        </div>
                    </div>


                </div>
                </section>
            @endforeach
<div class="container">


    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row" style="margin-top: 30px; margin-bottom: 30px;">
                <div class="col-md-6 text-left">
                    @if(!empty($previous))
                        <a href="/question/{{ $previous->id }}" class="btn btn-default btn-lg">
                            <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                            {{ $previous->title }}</a>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    @if(!empty($next))
                        <a href="/question/{{ $next->id }}" class="btn btn-default btn-lg">{{ $next->title }}
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                        </a>
                    @else
                        <a href="/downloads/DatabeeTime.pdf" class="btn btn-default btn-lg" target="_blank">
                            Download the full survey results
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
@endsection