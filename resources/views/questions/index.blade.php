@extends('layouts.survey')

@section('content')
        <section class="section-hero minor">
                <div class="hero-content">
                        <div class="row">
                                <div class="col-md-8 col-md-offset-2 text-center">
                                        <h1>The Questions</h1>

                                        <p class="lead">The survey questions were designed to target the key processes in the exams management workflow. Universities were asked how long these tasks took them before and after implementing Databee Exams Manager. The results are quite compelling!</p>

                                </div>
                        </div>
                </div>
        </section>

        <div class="container">
                <div class="row" style="padding-top: 30px;">
                        @foreach($questions as $question)
                        <div class="col-md-4" style="margin-bottom: 30px;">
                                        <a href="/question/{{ $question->id }}" style="display: block; position: relative; background-color: #{{ $question->colour }}; color: #fff; text-align: center; font-size: 1.5em;">
                                              <span style="display: block; padding: 50% 0">

                                              </span>


                                                <span style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%); width: 70%;">
                                                        {{ $question->title }}
                                                </span>
                                        </a>

                        </div>
                        @endforeach

                </div>

            <div class="row" style="padding-bottom: 30px;">
                <div class="col-md-6 col-md-offset-3">
                    <a href="/downloads/DatabeeTime.pdf" class="btn btn-default btn-block btn-lg" target="_blank">
                        <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                        Download the full survey results</a>
                </div>
            </div>

        </div>
@endsection