<section style="background-color: {{ lighten('#222222', 10) }}; color: #fff; padding: 30px 0; font-size: 1.2em">
    <div class="container text-center">
        <h3 style="margin-top: 0;">Want to find out more?</h3>

        <p>Discover how your university can achieve these efficiencies, and more, with an onsite demonstration.</p>

        <p>Contact Databee on 08 9401 8450 or at sales@databee.com.au</p>
    </div>
</section>
<footer class="page-footer">
    <div class="container text-center">
        <p>Copyright &copy; {{ date('Y') }} Databee Business Systems</p>
    </div>
</footer>