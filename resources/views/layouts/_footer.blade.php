<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-6">
                <h4>Company</h4>

                <ul class="list-unstyled">
                    <li><a href="/company/about">About</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Legal</a></li>
                </ul>
            </div>

            <div class="col-md-2 col-xs-6">
                <h4>Product</h4>

                <ul class="list-unstyled">
                    <li><a href="#">Scheduling</a></li>
                    <li><a href="#">Paper Management</a></li>
                    <li><a href="#">Supervisors</a></li>
                    <li><a href="#">Correspondence</a></li>
                    <li><a href="#">Reporting</a></li>
                    <li><a href="#">Websites</a></li>
                </ul>
            </div>

            <div class="col-md-2 col-xs-6">
                <h4>Learn More</h4>

                <ul class="list-unstyled">
                    <li><a href="#">Technology</a></li>
                    <li><a href="#">Pricing</a></li>
                    <li><a href="#">Customers</a></li>
                    <li><a href="#">Case Studies</a></li>
                    <li><a href="#">Procurement</a></li>
                    <li><a href="#">Contact Sales</a></li>
                </ul>
            </div>

            <div class="col-md-2 col-xs-6">
                <h4>Support</h4>

                <ul class="list-unstyled">
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Tickets</a></li>
                    <li><a href="#">Feature Requests</a></li>
                </ul>
            </div>

            <div class="col-md-2 col-xs-6">
                <h4>Community</h4>

                <ul class="list-unstyled">
                    <li><a href="#">User Guides</a></li>
                    <li><a href="/forum">Questions</a></li>
                    <li><a href="#">User Groups</a></li>
                </ul>
            </div>

            <div class="col-md-2 col-xs-6 copyright">
                <p>Copyright &copy; {{ date('Y') }}</p>
                <p>Databee Business Solutions</p>
                <p>Proudly made in 🇦🇺</p>
            </div>
        </div>

    </div>
</footer>