@extends('layouts.survey')

@section('content')
    <section class="section-hero minor">
        <div class="hero-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1>{{ $university->alias }}</h1>

                    @if($university->subtitle)
                    <p class="lead">{{ $university->subtitle }}</p>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <div class="container" style="padding-top: 30px;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">

                                <p class="lead">Examination Events per Year</p>

                                <h3 style="font-size: 2.5em">{{ $university->exam_events }}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">

                                <p class="lead">Exams per Examination Event</p>

                                <h3 style="font-size: 2.5em">{{ number_format($university->exams_per_event) }}</h3>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <p class="lead">Sittings per Examination Event</p>

                                <h3 style="font-size: 2.5em">{{ number_format($university->sittings_per_event) }}</h3>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <p class="lead">Full-time Staff</p>

                                <h3 style="font-size: 2.5em">{{ $university->staff }}</h3>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>




    </div>


    @php($q = $university->answers()->first()->question)
    <section style=" background-color: {{ lighten('#' . $q->colour, 30) }}; color: {{ darken('#' . $q->colour, 20) }}; padding-top: 15px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 text-center">Before Implementing<br>Databee Exams Manager</div>
                        <div class="col-md-4 text-center">After Implementing<br>Databee Exams Manager</div>
                    </div>

                    <?php
                    $total_before = 0;
                    $total_after = 0;
                    ?>
                </div>
            </div>
        </div>
    </section>

                @foreach($university->answers as $answer)
                    @php($question = $answer->question)
                    <section style="padding-top: 30px; padding-bottom: 15px; background-color: {{ lighten('#' . $question->colour, 30) }}">


                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">



                    <div class="row" style="display: flex; align-items: center" id="question{{ $answer->question->id }}">
                        <div class="col-md-4">
                            <h3 class="text-center" style="margin: 0;">

                                <a href="/question/{{ $answer->question->id }}" style="color: {{ darken('#' . $question->colour, 20) }}">{{ $answer->question->title }}</a>
                            </h3>
                        </div>

                        <div class="col-md-4" style="position: relative;">
                            <?php $total_before += $answer->totalDays('before'); ?>
                            @if(!empty($answer->before_duration_lower))
                            <div style="display: block;
                                    background: #{{ $question->colour }};
                                    border-radius: 100%;
                                    width: {{ $answer->relativeScale('before') }}%;
                                    margin: 0 auto;


                                    ">
                                <div style="padding: 50% 0;"></div>

                            </div>
                            <p class="lead text-center" style="font-size: 2em; position: absolute; margin: 0; top: 50%; left: 50%; transform: translate(-50%, -50%); color: #fff">

                                @foreach(range(1, $answer->before_staff) as $index)
                                    @if($index == 4)<br />@endif
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                @endforeach
                            </p>

                            @else
                            <div class="text-center" style="font-size: 3em; color: #{{ $question->colour }}">
                                <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                            </div>
                            @endif

                        </div>

                        <div class="col-md-4" style="position: relative;">
                            <?php $total_after += $answer->totalDays('after'); ?>

                            <div style="display: block;
                                    background: #{{ $question->colour }};
                                    border-radius: 100%;
                                    width: {{ $answer->relativeScale('after') }}%;
                                    margin: 0 auto;


                                    ">
                                <div style="padding: 50% 0;"></div>

                            </div>

                            <p class="lead text-center" style="font-size: 2em; position: absolute; margin: 0; top: 50%; left: 50%; transform: translate(-50%, -50%); color: #fff">
                                @foreach(range(1, $answer->after_staff) as $index)
                                    @if($index == 4)<br />@endif
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                @endforeach
                            </p>

                        </div>
                    </div>

                                </div>
                            </div>



                            <div class="row text-center" style="color: {{ darken('#' . $question->colour, 20) }}; margin-top: 5px;">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            @if(!empty($answer->before_duration_lower))
                                                {{ $answer->duration('before') }} / {{ $answer->staff('before') }} staff
                                            @endif
                                        </div>

                                        <div class="col-md-4">
                                            {{ $answer->duration('after') }} / {{ $answer->staff('after') }} staff
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>


                @endforeach

                <!--<p class="lead text-center">
                    Savings: <?php echo round($total_before - $total_after); ?> Days
                </p>-->

    <div class="container">
@if($university->comments)
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center">Further Comments</h2>

            {!! parse_markdown($university->comments) !!}
        </div>
    </div>
@endif

    <div class="row" style="margin-top: 30px; margin-bottom: 30px;">
        <div class="col-md-5 col-md-offset-1 text-left">
            @if(!empty($previous))
                <a href="/university/{{ $previous->id }}" class="btn btn-default btn-lg">
                    <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                    {{ $previous->alias }}</a>
            @endif
        </div>
        <div class="col-md-5 text-right">
            @if(!empty($next))
                <a href="/university/{{ $next->id }}" class="btn btn-default btn-lg">{{ $next->alias }}
                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                </a>
            @else
                <a href="/downloads/DatabeeTime.pdf" class="btn btn-default btn-lg" target="_blank">
                    Download the full survey results
                    <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                </a>
            @endif
        </div>
    </div>

</div>
@endsection