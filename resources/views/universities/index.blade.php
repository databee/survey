@extends('layouts.survey')

@section('content')
    <section class="section-hero minor">
        <div class="hero-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1>The Universities</h1>

                    <p class="lead">Databee currently services half of the universities in Australia, with clients in New Zealand and campuses in South East Asia.  Databee Exams Manager is currently powering the examinations offices of universities of all sizes.</p>

                </div>
            </div>
        </div>
    </section>

    <div class="container" style="padding-top: 30px;">

        <div class="row">
            @foreach($universities as $university)
                <div class="col-md-4" style="margin-bottom: 30px;">
                    <a href="/university/{{ $university->id }}" style="display: block; position: relative; background-color: {{ lighten('#222222') }}; color: #fff; text-align: center; font-size: 1.5em;">
                                              <span style="display: block; padding: 50% 0">

                                              </span>


                        <span style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%); width: 70%;">
                                                        {{ $university->alias }}
                                                </span>
                    </a>

                </div>
            @endforeach

        </div>

        <div class="row" style="padding-bottom: 30px;">
            <div class="col-md-6 col-md-offset-3">
                <a href="/downloads/DatabeeTime.pdf" class="btn btn-default btn-block btn-lg" target="_blank">
                    <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                    Download the full survey results</a>
            </div>
        </div>

    </div>
@endsection