@extends('layouts.survey')

@section('content')
    <section class="section-hero minor">
        <div class="hero-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1>Efficiency Through Innovation</h1>

                    <p class="lead">Discover how many of Australia’s top Universities are maximising their time efficiency with Databee Exams Manager.</p>

                </div>
            </div>
        </div>
    </section>

    <div class="container" style="padding-top: 30px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p class="lead">In 2016, an Australian University conducted	a survey of	Databee	Exams Manager Users.</p>

                <p>The survey assessed how long the Universities spent on key exam tasks before and after they implemented Databee Exams Manager. The results were surprising even for us!</p>

                <p>Clients have experienced significant benefits in terms of time, cost and quality assurance.</p>

                <p>This survey was conducted independently, without any involvement from Databee. The full results are available in this website. For privacy reasons the names of all the universities have been anonymised.</p>


                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-md-6">
                        <a href="/questions" class="btn btn-default btn-lg btn-block">See the Questions <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
                    </div>

                    <div class="col-md-6">
                        <a href="/universities" class="btn btn-default btn-lg btn-block">See the Universities <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>
                    </div>
                </div>


                <blockquote>
                    Exams Manager is more than just a record keeping system, and much more than a timetabling system. It is a comprehensive, process-based business system that will allow your exams office to work more efficiently and provide better service to your clients.
                </blockquote>
            </div>
        </div>

    </div>
@endsection